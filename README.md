# RPGitems Reloaded

[![Build Status](https://travis-ci.org/NyaaCat/RPGitems-reloaded.svg?branch=master)](https://travis-ci.org/NyaaCat/RPGitems-reloaded)

The RPGitems2 plugin continued from [TheCreeperOfRedstone/RPG-Items-2](https://github.com/TheCreeperOfRedstone/RPG-Items-2)

[Wiki](https://github.com/NyaaCat/RPGitems-reloaded/wiki) | [Spigot page](https://www.spigotmc.org/resources/rpgitems.17549/) | [Translation](https://www.transifex.com/phoenixlzx/rpgitems/)
